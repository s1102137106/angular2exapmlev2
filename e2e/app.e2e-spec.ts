import { AngularExampleV2Page } from './app.po';

describe('angular-example-v2 App', function() {
  let page: AngularExampleV2Page;

  beforeEach(() => {
    page = new AngularExampleV2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
