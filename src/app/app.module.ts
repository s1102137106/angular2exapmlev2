import { DialogComponent } from './custom/dialog-component';
import { WebApiUrlService } from './custom/webApiUrl';
import { ApiService } from './service/apiService';
import { DataService } from './service/dataService';

import { LoadingComponent } from './custom/loading-component';
import { HomeComponent } from './home/home.component';
import { OrderModule } from './order/orderModule';
import { AppRoutingModule } from './route/app-routing.module'
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material'
import { AppComponent } from './app.component';
import { ModalModule } from 'ng2-bootstrap/modal';

@NgModule({
  declarations: [
    AppComponent
    , HomeComponent
    , LoadingComponent
    , DialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    OrderModule,
    MaterialModule,
    ModalModule.forRoot(),

  ],
  providers: [DataService, ApiService, WebApiUrlService],
  bootstrap: [AppComponent]
})
export class AppModule { }
