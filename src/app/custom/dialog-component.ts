import { ViewChild, Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'dialog',
    templateUrl: './dialog.html'
})
export class DialogComponent implements OnInit {
    constructor() { }
    @ViewChild('dialogView') dialogView;

    private static dialogView;
    private static message = "訊息";
    private static title = "儲存成功";

    ngAfterViewInit() {
        DialogComponent.dialogView = this.dialogView;
    }

    public static setMessage(message: any) {
        DialogComponent.message = message;
    }

    public static setTitle(title: any) {
        DialogComponent.title = title;
    }

    //開啟
    public static show() {
        DialogComponent.dialogView.show();
    }

    //關閉
    public static close() {
        DialogComponent.dialogView.hide();

    }

    ngOnInit() { }
}