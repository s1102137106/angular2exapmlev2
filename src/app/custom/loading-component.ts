import { DialogComponent } from './dialog-component';
import { ViewChild, Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'loading',
    templateUrl: './loading.html'
})
export class LoadingComponent implements OnInit {
    constructor() { }
    @ViewChild('loadingView') loadingView;

    private static loadingView;
    private static waitLoad = 0;//等待連接後台個數

    ngAfterViewInit() {
        LoadingComponent.loadingView = this.loadingView;
    }

    //開啟Loading畫面
    static startLoading() {
        LoadingComponent.waitLoad = LoadingComponent.waitLoad + 1;
        LoadingComponent.loadingView.show();
    }

    //關閉Loading畫面
    static stopLoading() {
        LoadingComponent.waitLoad = LoadingComponent.waitLoad - 1;
        
        //當全部完成時才會關閉Loading畫面
        if (LoadingComponent.waitLoad <= 0) {
            LoadingComponent.loadingView.hide();
        }
    }

    ngOnInit() { }
}