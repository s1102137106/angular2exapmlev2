export enum WebApiUrlType {
    EmployeeList,//employee下拉式選單
    CustomerList,
    ProductList,
    ShipperList,
    OrderResult,
    Order,
    OrderDetail,
    GetOrderDetail,//api遇到些問題 暫時獨立寫一支
    DeleteOrderDetail//api遇到些問題 暫時獨立寫一支
}
import { Injectable } from '@angular/core';

@Injectable()
export class WebApiUrlService {
    private webUrl = "http://localhost:59674/"

    constructor() { }

    //取得api URL
    getUrlString(urlType: WebApiUrlType): string {

        switch (urlType) {
            case WebApiUrlType.EmployeeList:
                return this.webUrl + "odata/Employees";
            case WebApiUrlType.CustomerList:
                return this.webUrl + "odata/Customers";
            case WebApiUrlType.ProductList:
                return this.webUrl + "odata/Products";
            case WebApiUrlType.ShipperList:
                return this.webUrl + "odata/Shippers";
            case WebApiUrlType.OrderResult:
                return this.webUrl + "api/Order";
            case WebApiUrlType.OrderDetail:
                return this.webUrl + "odata/OdataOrderDetail";

        }
    }

    //取得api URL 有參數
    getUrlStringById(urlType: WebApiUrlType, id: any): string {

        switch (urlType) {
            case WebApiUrlType.Order:
                return this.webUrl + "odata/OdataOrder(" + id + ")";
            case WebApiUrlType.GetOrderDetail:
                return this.webUrl + "odata/OdataOrder(" + id + ")/OrderDetails";
            case WebApiUrlType.DeleteOrderDetail:
                return this.webUrl + "odata/OdataOrderDetail?id=" + id;
        }
    }
}