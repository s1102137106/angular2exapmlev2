import { ApiService } from './../service/apiService';
import { LoadingComponent } from './../custom/loading-component';
import { DataService } from './../service/dataService';
import { ConditionModel } from './conditionModel';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { WebApiUrlType } from './../custom/webApiUrl';



@Component({
    moduleId: module.id,
    selector: 'order-condition',
    templateUrl: 'order-condition.html'
})
export class OrderConditionComponent implements OnInit {

    constructor(private apiService: ApiService) { }

    @Output() searchResultUpdated = new EventEmitter();

    employeeList;
    shipperList;
    errorMessage;
    model: ConditionModel;

    ngOnInit() {
        this.model = new ConditionModel(null);
        this.apiService.getData(WebApiUrlType.EmployeeList, (result => this.employeeList = result));
        this.apiService.getData(WebApiUrlType.ShipperList, (result => this.shipperList = result));
        this.onSubmit();//init
    }

    //取得condition 並且請master 更新Result
    onSubmit() {
        this.searchResultUpdated.emit(this.model);
    }




}