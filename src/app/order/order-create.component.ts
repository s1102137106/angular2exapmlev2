import { Location } from '@angular/common';
import { WebApiUrlType } from '../custom/webApiUrl';
import { OrderModel } from './orderModel';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from './../service/apiService';
import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'order-create',
    templateUrl: './order-create.html'
})
export class OrderCreateComponent implements OnInit {
    constructor(
        private apiService: ApiService, private location: Location)
    { this.order = new OrderModel() }

    order: OrderModel;//訂單資料
    orderDetail;//訂單明細
    employeeList;//Employee下拉式選單
    shipperList;//Shipper下拉式選單
    customers;//客戶下拉式選單
    products;//產品下拉式選單

    ngOnInit() {
        this.apiService.getData(WebApiUrlType.EmployeeList, (result => this.employeeList = result));
        this.apiService.getData(WebApiUrlType.ShipperList, (result => this.shipperList = result));
        this.apiService.getData(WebApiUrlType.CustomerList, (result => this.customers = result));
        this.apiService.getData(WebApiUrlType.ProductList, (result => this.products = result));

    }
}