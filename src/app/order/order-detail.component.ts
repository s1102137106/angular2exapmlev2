import { ApiService } from './../service/apiService';
import { WebApiUrlType } from '../custom/webApiUrl';
import { OrderModel } from './orderModel';


import { Location } from '@angular/common';
import { Params, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'detail',
    templateUrl: './order-detail.html'
})
export class OrderDetailComponent implements OnInit {

    constructor(
        private apiService: ApiService,
        private route: ActivatedRoute,
        private location: Location)
    { this.order = new OrderModel() }

    order: OrderModel;//訂單資料
    orderDetail;//訂單明細
    employeeList;//Employee下拉式選單
    shipperList;//Shipper下拉式選單
    customers;//客戶下拉式選單
    products;//產品下拉式選單

    private title = '修改訂單'
    private subTitle = '訂單明細'

    private errorMessage;

    ngOnInit() {
        this.apiService.getData(WebApiUrlType.EmployeeList, (result => this.employeeList = result));
        this.apiService.getData(WebApiUrlType.ShipperList, (result => this.shipperList = result));
        this.apiService.getData(WebApiUrlType.CustomerList, (result => this.customers = result));
        this.apiService.getData(WebApiUrlType.ProductList, (result => this.products = result));
        this.route.params
            .subscribe(params => this.getData(params));
    }

    //得到頁面所需要的資源
    getData(params: Params) {
        var orderId = +params['orderId'];
        this.apiService.getDataById(WebApiUrlType.Order, orderId, (order => this.orderComplete(order)))
        this.apiService.getDataById(WebApiUrlType.GetOrderDetail, orderId, (orderDetail => this.orderDetail = orderDetail));
    }

    //取得訂單資料後 將日期format
    public orderComplete(order) {
        this.order = order;
        this.order.orderdate = this.dateFormat(this.order.orderdate);
        this.order.shippeddate = this.dateFormat(this.order.shippeddate);
        this.order.requireddate = this.dateFormat(this.order.requireddate);
    }

    public dateFormat(date: string) {
        return date.toString().substring(0, 10);
    }

    onSubmit() {
        //put order 修改訂單
        this.apiService.putDataById(WebApiUrlType.Order, this.order.orderid, this.order, (res) => console.log("Order save success"));

        //delete orderdetail
        this.apiService.deleteDataById(WebApiUrlType.DeleteOrderDetail, this.order.orderid, ((res) => this.completeDeleteOrderDetails()))
    }

    //完成刪除 在將所有資料新增上去
    completeDeleteOrderDetails() {
        //insert orderdetail
        for (var item in this.orderDetail) {
            this.apiService.putData(WebApiUrlType.OrderDetail, this.orderDetail[item], ((ordee) => console.log("Order detail save success")))
        }
    }






}