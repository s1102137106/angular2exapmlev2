import { ApiService } from './../service/apiService';
import { LoadingComponent } from './../custom/loading-component';
import { WebApiUrlType } from '../custom/webApiUrl';
import { DataService } from './../service/dataService';
import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'order-master',
    templateUrl: './order-master.html'
})
export class OrderMasterComponent implements OnInit {

    constructor(private apiService: ApiService) { }

    searchResult;//搜尋結果
    errorMessage;
    condition;//搜尋條件

    //condition.ts 得到 condition 後呼叫此函數取的Result
    setSearchResult(condition) {
        this.condition = condition;
        this.apiService.postData(WebApiUrlType.OrderResult, condition, (searchResult => this.searchResult = searchResult));
    }

    //將condition重新送出
    reLoadSearchResult() {
        this.apiService.postData(WebApiUrlType.OrderResult, this.condition, (searchResult => this.searchResult = searchResult));
    }


    ngOnInit() { }
}