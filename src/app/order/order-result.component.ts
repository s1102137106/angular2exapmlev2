import { WebApiUrlService } from './../custom/webApiUrl';
import { ApiService } from './../service/apiService';
import { WebApiUrlType } from '../custom/webApiUrl';
import { Input, Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';

@Component({
    moduleId: module.id,
    selector: 'order-result',
    templateUrl: './order-result.html'
})
export class OrderResultComponent implements OnInit {
    constructor(private apiService: ApiService, private location: Location) { }
    @Input() searchResult;
    @Output() reSetSearchResult = new EventEmitter() ;

    selectOrderId: number;

    deleteOrder(orderID: number) {

        this.selectOrderId = orderID;

        //delete orderdetail
        this.apiService.deleteDataById(WebApiUrlType.DeleteOrderDetail, orderID, ((res) => this.completeDeleteOrderDetails()))

    }

    completeDeleteOrderDetails() {
        //刪除完成後 重新取得SearchResult
        this.apiService.deleteDataById(WebApiUrlType.Order, this.selectOrderId, res => this.reSetSearchResult.emit() );
    }






    ngOnInit() { }

}