export class OrderModel {
    constructor(
        public orderid?: number,
        public empid?: number,
        public orderdate?: string,
        public requireddate?:string,
        public shipperid?: number,
        public freight?: number,
        public shipname?: string,
        public shipaddress?: string,
        public shipcity?: string,
        public shipregion?: string,
        public shippostalcode?: string,
        public shipcountry?: string,
        public custid?: number,
        public shippeddate?: string,
    ) { }
}
