import { OrderCreateComponent } from './order-create.component';
import { AppRoutingModule } from './../route/app-routing.module';
import { OrderDetailComponent } from './order-detail.component';
import { OrderResultComponent } from './order-result.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { OrderMasterComponent } from './order-master.componen';
import { OrderConditionComponent } from './order-condition.component';
import { NgModule } from '@angular/core';


@NgModule({
    imports: [BrowserModule, FormsModule, AppRoutingModule],
    exports: [],
    declarations: [
        OrderConditionComponent,
        OrderMasterComponent,
        OrderResultComponent,
        OrderDetailComponent,
        OrderCreateComponent
    ],
    providers: [],
})

export class OrderModule { }
