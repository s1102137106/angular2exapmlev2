import { OrderCreateComponent } from './../order/order-create.component';
import { OrderDetailComponent } from './../order/order-detail.component';
import { HomeComponent } from './../home/home.component';

import { OrderMasterComponent } from './../order/order-master.componen';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';



const routes: Routes = [
    { path: '', redirectTo: '/Home', pathMatch: 'full' },
    { path: 'Home', component: HomeComponent },
    { path: 'Order', component: OrderMasterComponent },
    { path: 'orderDetail/:orderId', component: OrderDetailComponent },
    { path: 'orderCreate', component: OrderCreateComponent }
    
    

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }