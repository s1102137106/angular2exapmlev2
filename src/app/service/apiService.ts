import { LoadingComponent } from './../custom/loading-component';
import { WebApiUrlType } from '../custom/webApiUrl';
import { DataService } from './dataService';
import { WebApiUrlService } from './../custom/webApiUrl';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiService {

    constructor(private urlService: WebApiUrlService, private dataService: DataService) { }

    private errorMessage;

    /**
     * 
     * @param apiType 呼叫後台種類
     * @param setValue 委派 得到資料後要做的事情由外面傳進來的函數決定 (callback)
     */
    public getData(apiType: WebApiUrlType, setValue: any) {
        LoadingComponent.startLoading();
        var apiUrl = this.urlService.getUrlString(apiType);
        this.dataService.getRequest(apiUrl).subscribe(
            setValue,
            error => this.errorMessage = <any>error,
            () => LoadingComponent.stopLoading());
    }

    /**
  * 
  * @param apiType 
  * @param id odata key
  * @param setValue callback
  */
    public getDataById(apiType: WebApiUrlType, id: any, setValue: any) {
        LoadingComponent.startLoading();
        var apiUrl = this.urlService.getUrlStringById(apiType, id);
        this.dataService.getRequest(apiUrl).subscribe(
            setValue,
            error => this.errorMessage = <any>error,
            () => LoadingComponent.stopLoading());
    }

    public postData(apiType: WebApiUrlType, data: any, setValue: any) {
        LoadingComponent.startLoading();
        var apiUrl = this.urlService.getUrlString(apiType);
        this.dataService.postRequest(apiUrl, data).subscribe(
            setValue,
            error => this.errorMessage = <any>error,
            () => LoadingComponent.stopLoading());
    }

    public putData(apiType: WebApiUrlType, data: any, setValue: any) {
        LoadingComponent.startLoading();
        var apiUrl = this.urlService.getUrlString(apiType);
        this.dataService.putRequest(apiUrl, data).subscribe(
            setValue,
            error => this.errorMessage = <any>error,
            () => LoadingComponent.stopLoading());
    }

    public putDataById(apiType: WebApiUrlType, id: any, data: any, setValue: any) {
        LoadingComponent.startLoading();
        var apiUrl = this.urlService.getUrlStringById(apiType, id);
        this.dataService.putRequest(apiUrl, data).subscribe(
            setValue,
            error => this.errorMessage = <any>error,
            () => LoadingComponent.stopLoading());
    }

    public deleteData(apiType: WebApiUrlType, setValue: any) {
        LoadingComponent.startLoading();
        var apiUrl = this.urlService.getUrlString(apiType);
        this.dataService.deleteRequest(apiUrl).subscribe(
            setValue,
            error => this.errorMessage = <any>error,
            () => LoadingComponent.stopLoading());
    }

    public deleteDataById(apiType: WebApiUrlType, id: any, setValue: any) {
        LoadingComponent.startLoading();
        var apiUrl = this.urlService.getUrlStringById(apiType, id);
        this.dataService.deleteRequest(apiUrl).subscribe(
            setValue,
            error => this.errorMessage = <any>error,
            () => LoadingComponent.stopLoading());
    }


}