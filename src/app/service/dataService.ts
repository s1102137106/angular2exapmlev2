import { Response, Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';



@Injectable()
export class DataService {


    private headers;
    private options;

    constructor(private http: Http) {
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        this.options = new RequestOptions({ headers: this.headers });
    }

    getRequest(webApiUrl: string): Observable<any> {
        return this.http.get(webApiUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }

    postRequest(webApiUrl: string, data: any): Observable<any> {
        return this.http.post(webApiUrl, JSON.stringify(data), this.options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    putRequest(webApiUrl: string, data: any): Observable<any> {
        return this.http.put(webApiUrl, JSON.stringify(data), this.options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    deleteRequest(webApiUrl: string): Observable<any> {
        return this.http.delete(webApiUrl, this.options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        
        let body = res.json();
        if (body == null) {
            return null;
        }
        if (body.value) {
            return body.value
        }
        return body || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
